//
// Created by dan on 07.11.2019.
//

#ifndef LIBRARY_SQLITE_EXT_SQLESTATEMENT_H
#define LIBRARY_SQLITE_EXT_SQLESTATEMENT_H

#include "sqlemain.h"
#include "sqleerror.h"

G_BEGIN_DECLS

typedef gpointer (*SQLEStatementExecuteCallback)(sqlite3_stmt *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(sqlite3_stmt, sqlite3_finalize)

gint sqle_statement_bind_blob(sqlite3_stmt *self, gint index, gpointer value, gint n, GDestroyNotify destructor, GError **error);
gint sqle_statement_bind_double(sqlite3_stmt *self, gint index, gdouble value, GError **error);
gint sqle_statement_bind_int(sqlite3_stmt *self, gint index, gint value, GError **error);
gint sqle_statement_bind_int64(sqlite3_stmt *self, gint index, gint64 value, GError **error);
gint sqle_statement_bind_text(sqlite3_stmt *self, gint index, gchar *value, gint n, GDestroyNotify destructor, GError **error);
gint sqle_statement_bind_pointer(sqlite3_stmt *self, gint index, gpointer value, gchar *type, GDestroyNotify destructor, GError **error);

gint sqle_statement_step(sqlite3_stmt *self, GError **error);
gint sqle_statement_execute(sqlite3_stmt *self, GList **rows, SQLEStatementExecuteCallback callback, GCancellable *cancellable, GError **error);

gpointer sqle_statement_int(sqlite3_stmt *self);
gpointer sqle_statement_text(sqlite3_stmt *self);

G_END_DECLS

#endif //LIBRARY_SQLITE_EXT_SQLESTATEMENT_H
