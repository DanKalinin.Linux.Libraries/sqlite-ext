//
// Created by dan on 30.10.2019.
//

#include "sqleconnection.h"










gint sqle_connection_open(gchar *file, sqlite3 **self, GError **error) {
    gint ret = sqlite3_open(file, self);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_connection_prepare_v2(sqlite3 *self, gchar *sql, gint n, sqlite3_stmt **statement, gchar **tail, GError **error) {
    gint ret = sqlite3_prepare_v2(self, sql, n, statement, (const gchar **)tail);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_connection_exec(sqlite3 *self, gchar *sql, SQLEConnectionExecCallback callback, gpointer data, gchar **message, GError **error) {
    gint ret = sqlite3_exec(self, sql, callback, data, message);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_connection_busy_timeout(sqlite3 *self, gint ms, GError **error) {
    gint ret = sqlite3_busy_timeout(self, ms);
    
    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }
    
    return ret;
}

gint sqle_connection_preupdate_old(sqlite3 *self, gint column, sqlite3_value **value, GError **error) {
    gint ret = sqlite3_preupdate_old(self, column, value);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_connection_preupdate_new(sqlite3 *self, gint column, sqlite3_value **value, GError **error) {
    gint ret = sqlite3_preupdate_new(self, column, value);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_connection_preupdate_value(sqlite3 *self, gboolean new, gint column, sqlite3_value **value, GError **error) {
    gint ret = SQLITE_OK;

    if (new) {
        ret = sqle_connection_preupdate_new(self, column, value, error);
    } else {
        ret = sqle_connection_preupdate_old(self, column, value, error);
    }

    return ret;
}

gint sqle_connection_select_int(sqlite3 *self, gchar *tail, GList **ints, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(self, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, ints, sqle_statement_int, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}

gint sqle_connection_select_text(sqlite3 *self, gchar *tail, GList **texts, GError **error) {
    gint ret = SQLITE_OK;
    g_autofree gchar *sql = g_strjoin(" ", "SELECT", tail, NULL);
    g_autoptr(sqlite3_stmt) statement = NULL;
    if ((ret = sqle_connection_prepare_v2(self, sql, -1, &statement, NULL, error)) != SQLITE_OK) return ret;
    if ((ret = sqle_statement_execute(statement, texts, sqle_statement_text, NULL, error)) != SQLITE_DONE) return ret;
    return ret;
}










enum {
    SQLE_CONNECTION_SIGNAL_UPDATE = 1,
    SQLE_CONNECTION_SIGNAL_PREUPDATE,
    SQLE_CONNECTION_SIGNAL_COMMIT,
    SQLE_CONNECTION_SIGNAL_WAL,
    _SQLE_CONNECTION_SIGNAL_COUNT
};

guint sqle_connection_signals[_SQLE_CONNECTION_SIGNAL_COUNT] = {0};

void sqle_connection_finalize(GObject *self);
void _sqle_connection_update(SQLEConnection *self, gint operation, gchar *schema, gchar *table, gint64 rowid);
void _sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid);
gint _sqle_connection_commit(SQLEConnection *self);
gint _sqle_connection_wal(SQLEConnection *self, sqlite3 *database, gchar *schema, gint pages);

G_DEFINE_TYPE(SQLEConnection, sqle_connection, GE_TYPE_OBJECT)

static void sqle_connection_class_init(SQLEConnectionClass *class) {
    G_OBJECT_CLASS(class)->finalize = sqle_connection_finalize;

    class->update = _sqle_connection_update;
    class->preupdate = _sqle_connection_preupdate;
    class->commit = _sqle_connection_commit;
    class->wal = _sqle_connection_wal;

    sqle_connection_signals[SQLE_CONNECTION_SIGNAL_UPDATE] = g_signal_new("update", SQLE_TYPE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SQLEConnectionClass, update), NULL, NULL, NULL, G_TYPE_NONE, 4, G_TYPE_INT, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_INT64);
    sqle_connection_signals[SQLE_CONNECTION_SIGNAL_PREUPDATE] = g_signal_new("preupdate", SQLE_TYPE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SQLEConnectionClass, preupdate), NULL, NULL, NULL, G_TYPE_NONE, 6, G_TYPE_POINTER, G_TYPE_INT, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_INT64, G_TYPE_INT64);
    sqle_connection_signals[SQLE_CONNECTION_SIGNAL_COMMIT] = g_signal_new("commit", SQLE_TYPE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SQLEConnectionClass, commit), NULL, NULL, NULL, G_TYPE_INT, 0);
    sqle_connection_signals[SQLE_CONNECTION_SIGNAL_WAL] = g_signal_new("wal", SQLE_TYPE_CONNECTION, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SQLEConnectionClass, wal), NULL, NULL, NULL, G_TYPE_INT, 3, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_INT);
}

static void sqle_connection_init(SQLEConnection *self) {

}

void sqle_connection_finalize(GObject *self) {
    sqle_connection_set_object(SQLE_CONNECTION(self), NULL);

    G_OBJECT_CLASS(sqle_connection_parent_class)->finalize(self);
}

void sqle_connection_update_callback(gpointer self, gint operation, const gchar *schema, const gchar *table, sqlite3_int64 rowid) {
    sqle_connection_update(self, operation, (gchar *)schema, (gchar *)table, rowid);
}

void sqle_connection_update(SQLEConnection *self, gint operation, gchar *schema, gchar *table, gint64 rowid) {
    g_signal_emit(self, sqle_connection_signals[SQLE_CONNECTION_SIGNAL_UPDATE], 0, operation, schema, table, rowid);
}

void _sqle_connection_update(SQLEConnection *self, gint operation, gchar *schema, gchar *table, gint64 rowid) {
    g_print("update\n");
}

void sqle_connection_preupdate_callback(gpointer self, sqlite3 *database, gint operation, const gchar *schema, const gchar *table, sqlite3_int64 rowid, sqlite3_int64 new_rowid) {
    sqle_connection_preupdate(self, database, operation, (gchar *)schema, (gchar *)table, rowid, new_rowid);
}

void sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid) {
    g_signal_emit(self, sqle_connection_signals[SQLE_CONNECTION_SIGNAL_PREUPDATE], 0, database, operation, schema, table, rowid, new_rowid);
}

void _sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid) {
    g_print("preupdate\n");
}

gint sqle_connection_commit_callback(gpointer self) {
    gint ret = sqle_connection_commit(self);
    return ret;
}

gint sqle_connection_commit(SQLEConnection *self) {
    gint ret = SQLITE_OK;
    g_signal_emit(self, sqle_connection_signals[SQLE_CONNECTION_SIGNAL_COMMIT], 0, &ret);
    return ret;
}

gint _sqle_connection_commit(SQLEConnection *self) {
    g_print("commit\n");
    return SQLITE_OK;
}

gint sqle_connection_wal_callback(gpointer self, sqlite3 *database, const gchar *schema, gint pages) {
    gint ret = sqle_connection_wal(self, database, (gchar *)schema, pages);
    return ret;
}

gint sqle_connection_wal(SQLEConnection *self, sqlite3 *database, gchar *schema, gint pages) {
    gint ret = SQLITE_OK;
    g_signal_emit(self, sqle_connection_signals[SQLE_CONNECTION_SIGNAL_WAL], 0, database, schema, pages, &ret);
    return ret;
}

gint _sqle_connection_wal(SQLEConnection *self, sqlite3 *database, gchar *schema, gint pages) {
    g_print("wal\n");
    return SQLITE_OK;
}
