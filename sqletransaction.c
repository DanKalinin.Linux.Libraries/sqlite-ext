//
// Created by root on 04.07.2020.
//

#include "sqletransaction.h"










SQLETransaction *sqle_transaction_new(sqlite3 *connection, GError **error) {
    if (sqle_connection_exec(connection, "BEGIN TRANSACTION", NULL, NULL, NULL, error) != SQLITE_OK) return NULL;

    SQLETransaction *self = g_new0(SQLETransaction, 1);
    self->connection = connection;
    return self;
}

void sqle_transaction_free(SQLETransaction *self) {
    g_free(self);
}

gint sqle_transaction_commit(SQLETransaction *self, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_connection_exec(self->connection, "COMMIT", NULL, NULL, NULL, error)) != SQLITE_OK) return ret;
    return ret;
}

gint sqle_transaction_rollback(SQLETransaction *self, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_connection_exec(self->connection, "ROLLBACK", NULL, NULL, NULL, error)) != SQLITE_OK) return ret;
    return ret;
}










gint sqle_auto_transaction_commit(SQLEAutoTransaction **self, GError **error) {
    gint ret = SQLITE_OK;
    if ((ret = sqle_transaction_commit(*self, error)) != SQLITE_OK) return ret;
    g_clear_pointer((gpointer *)self, sqle_transaction_free);
    return ret;
}

void sqle_auto_transaction_rollback(SQLEAutoTransaction *self) {
    (void)sqle_transaction_rollback(self, NULL);
    sqle_transaction_free(self);
}
