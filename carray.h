//
// Created by root on 19.07.2020.
//

#ifndef LIBRARY_SQLITE_EXT_CARRAY_H
#define LIBRARY_SQLITE_EXT_CARRAY_H

#include <sqlite3.h>

int sqlite3_carray_init(sqlite3 *db, char **pzErrMsg, const sqlite3_api_routines *pApi);

#endif //LIBRARY_SQLITE_EXT_CARRAY_H
