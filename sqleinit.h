//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_SQLITE_EXT_SQLEINIT_H
#define LIBRARY_SQLITE_EXT_SQLEINIT_H

#include "sqlemain.h"

G_BEGIN_DECLS

void sqle_init(void);

G_END_DECLS

#endif //LIBRARY_SQLITE_EXT_SQLEINIT_H
