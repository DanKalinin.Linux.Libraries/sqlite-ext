//
// Created by dan on 31.10.2019.
//

#ifndef LIBRARY_SQLITE_EXT_SQLEERROR_H
#define LIBRARY_SQLITE_EXT_SQLEERROR_H

#include "sqlemain.h"

G_BEGIN_DECLS

#define SQLE_ERROR sqle_error_quark()

GQuark sqle_error_quark(void);

GError *sqle_error_new(gint code);
void sqle_set_error(GError **self, gint code);

G_END_DECLS

#endif //LIBRARY_SQLITE_EXT_SQLEERROR_H
