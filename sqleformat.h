//
// Created by dan on 21.01.2022.
//

#ifndef LIBRARY_SQLITE_EXT_SQLEFORMAT_H
#define LIBRARY_SQLITE_EXT_SQLEFORMAT_H

#include "sqlemain.h"

G_BEGIN_DECLS

gchar *sqle_format_text(gchar *self);
gchar *sqle_format_texts(GList *self);
gchar *sqle_format_ints(GList *self);

G_END_DECLS

#endif //LIBRARY_SQLITE_EXT_SQLEFORMAT_H
