//
// Created by dan on 30.10.2019.
//

#ifndef LIBRARY_SQLITE_EXT_SQLECONNECTION_H
#define LIBRARY_SQLITE_EXT_SQLECONNECTION_H

#include "sqlemain.h"
#include "sqleerror.h"
#include "sqlestatement.h"










G_BEGIN_DECLS

typedef gint (*SQLEConnectionExecCallback)(gpointer data, gint n, gchar **texts, gchar **names);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(sqlite3, sqlite3_close_v2)

gint sqle_connection_open(gchar *file, sqlite3 **self, GError **error);
gint sqle_connection_prepare_v2(sqlite3 *self, gchar *sql, gint n, sqlite3_stmt **statement, gchar **tail, GError **error);
gint sqle_connection_exec(sqlite3 *self, gchar *sql, SQLEConnectionExecCallback callback, gpointer data, gchar **message, GError **error);
gint sqle_connection_busy_timeout(sqlite3 *self, gint ms, GError **error);

gint sqle_connection_preupdate_old(sqlite3 *self, gint column, sqlite3_value **value, GError **error);
gint sqle_connection_preupdate_new(sqlite3 *self, gint column, sqlite3_value **value, GError **error);
gint sqle_connection_preupdate_value(sqlite3 *self, gboolean new, gint column, sqlite3_value **value, GError **error);

gint sqle_connection_select_int(sqlite3 *self, gchar *tail, GList **ints, GError **error);
gint sqle_connection_select_text(sqlite3 *self, gchar *tail, GList **texts, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define SQLE_TYPE_CONNECTION sqle_connection_get_type()

GE_DECLARE_DERIVABLE_TYPE(SQLEConnection, sqle_connection, SQLE, CONNECTION, GEObject)

struct _SQLEConnectionClass {
    GEObjectClass super;

    void (*update)(SQLEConnection *self, gint operation, gchar *schema, gchar *table, gint64 rowid);
    void (*preupdate)(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid);
    gint (*commit)(SQLEConnection *self);
    gint (*wal)(SQLEConnection *self, sqlite3 *database, gchar *schema, gint pages);
};

struct _SQLEConnection {
    GEObject super;

    sqlite3 *object;
};

GE_STRUCTURE_FIELD(sqle_connection, object, SQLEConnection, sqlite3, NULL, sqlite3_close_v2, NULL)

void sqle_connection_update_callback(gpointer self, gint operation, const gchar *schema, const gchar *table, sqlite3_int64 rowid);
void sqle_connection_update(SQLEConnection *self, gint operation, gchar *schema, gchar *table, gint64 rowid);

void sqle_connection_preupdate_callback(gpointer self, sqlite3 *database, gint operation, const gchar *schema, const gchar *table, sqlite3_int64 rowid, sqlite3_int64 new_rowid);
void sqle_connection_preupdate(SQLEConnection *self, sqlite3 *database, gint operation, gchar *schema, gchar *table, gint64 rowid, gint64 new_rowid);

gint sqle_connection_commit_callback(gpointer self);
gint sqle_connection_commit(SQLEConnection *self);

gint sqle_connection_wal_callback(gpointer self, sqlite3 *database, const gchar *schema, gint pages);
gint sqle_connection_wal(SQLEConnection *self, sqlite3 *database, gchar *schema, gint pages);

G_END_DECLS










#endif //LIBRARY_SQLITE_EXT_SQLECONNECTION_H
