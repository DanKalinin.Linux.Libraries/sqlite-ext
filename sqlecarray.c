//
// Created by root on 20.07.2020.
//

#include "sqlecarray.h"

gint sqle_carray_init(sqlite3 *self, gchar **message, sqlite3_api_routines *api, GError **error) {
    gint ret = sqlite3_carray_init(self, message, api);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_carray_bind_pointer(sqlite3_stmt *self, gint index, gpointer value, GDestroyNotify destructor, GError **error) {
    gint ret = sqle_statement_bind_pointer(self, index, value, "carray", destructor, error);
    return ret;
}
