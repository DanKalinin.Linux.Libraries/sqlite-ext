//
// Created by root on 20.07.2020.
//

#ifndef LIBRARY_SQLITE_EXT_SQLECARRAY_H
#define LIBRARY_SQLITE_EXT_SQLECARRAY_H

#include "sqlemain.h"
#include "sqleerror.h"
#include "sqlestatement.h"

G_BEGIN_DECLS

gint sqle_carray_init(sqlite3 *self, gchar **message, sqlite3_api_routines *api, GError **error);
gint sqle_carray_bind_pointer(sqlite3_stmt *self, gint index, gpointer value, GDestroyNotify destructor, GError **error);

G_END_DECLS

#endif //LIBRARY_SQLITE_EXT_SQLECARRAY_H
