//
// Created by root on 04.07.2020.
//

#ifndef LIBRARY_SQLITE_EXT_SQLETRANSACTION_H
#define LIBRARY_SQLITE_EXT_SQLETRANSACTION_H

#include "sqlemain.h"
#include "sqleconnection.h"










G_BEGIN_DECLS

typedef struct {
    sqlite3 *connection;
} SQLETransaction;

SQLETransaction *sqle_transaction_new(sqlite3 *connection, GError **error);
void sqle_transaction_free(SQLETransaction *self);
gint sqle_transaction_commit(SQLETransaction *self, GError **error);
gint sqle_transaction_rollback(SQLETransaction *self, GError **error);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SQLETransaction, sqle_transaction_free)

G_END_DECLS










G_BEGIN_DECLS

typedef SQLETransaction SQLEAutoTransaction;

gint sqle_auto_transaction_commit(SQLEAutoTransaction **self, GError **error);
void sqle_auto_transaction_rollback(SQLEAutoTransaction *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(SQLEAutoTransaction, sqle_auto_transaction_rollback)

G_END_DECLS










#endif //LIBRARY_SQLITE_EXT_SQLETRANSACTION_H
