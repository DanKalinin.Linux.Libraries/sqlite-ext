//
// Created by dan on 07.11.2019.
//

#include "sqlestatement.h"

gint sqle_statement_bind_blob(sqlite3_stmt *self, gint index, gpointer value, gint n, GDestroyNotify destructor, GError **error) {
    gint ret = sqlite3_bind_blob(self, index, value, n, destructor);
    
    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }
    
    return ret;
}

gint sqle_statement_bind_double(sqlite3_stmt *self, gint index, gdouble value, GError **error) {
    gint ret = sqlite3_bind_double(self, index, value);
    
    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }
    
    return ret;
}

gint sqle_statement_bind_int(sqlite3_stmt *self, gint index, gint value, GError **error) {
    gint ret = sqlite3_bind_int(self, index, value);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_statement_bind_int64(sqlite3_stmt *self, gint index, gint64 value, GError **error) {
    gint ret = sqlite3_bind_int64(self, index, value);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_statement_bind_text(sqlite3_stmt *self, gint index, gchar *value, gint n, GDestroyNotify destructor, GError **error) {
    gint ret = sqlite3_bind_text(self, index, value, n, destructor);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_statement_bind_pointer(sqlite3_stmt *self, gint index, gpointer value, gchar *type, GDestroyNotify destructor, GError **error) {
    gint ret = sqlite3_bind_pointer(self, index, value, type, destructor);

    if (ret != SQLITE_OK) {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_statement_step(sqlite3_stmt *self, GError **error) {
    gint ret = sqlite3_step(self);

    if (ret == SQLITE_ROW) {
    } else if (ret == SQLITE_DONE) {
    } else {
        sqle_set_error(error, ret);
    }

    return ret;
}

gint sqle_statement_execute(sqlite3_stmt *self, GList **rows, SQLEStatementExecuteCallback callback, GCancellable *cancellable, GError **error) {
    gint ret = SQLITE_DONE;

    while ((ret = sqle_statement_step(self, error)) == SQLITE_ROW) {
        gpointer row = callback(self);
        *rows = g_list_append(*rows, row);
    }

    return ret;
}

gpointer sqle_statement_int(sqlite3_stmt *self) {
    gint ret = sqlite3_column_int(self, 0);
    return GINT_TO_POINTER(ret);
}

gpointer sqle_statement_text(sqlite3_stmt *self) {
    gchar *ret = (gchar *)sqlite3_column_text(self, 0);
    return g_strdup(ret);
}
