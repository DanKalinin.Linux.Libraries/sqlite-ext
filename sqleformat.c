//
// Created by dan on 21.01.2022.
//

#include "sqleformat.h"

gchar *sqle_format_text(gchar *self) {
    gchar *ret = g_strdup_printf("'%s'", self);
    return ret;
}

gchar *sqle_format_texts(GList *self) {
    GString *ret = g_string_new(NULL);
    
    for (GList *value = self; value != NULL; value = value->next) {
        (void)g_string_append_printf(ret, ",'%s'", (gchar *)value->data);
    }
    
    (void)g_string_overwrite(ret, 0, "(");
    (void)g_string_append(ret, ")");
    return g_string_free(ret, FALSE);
}

gchar *sqle_format_ints(GList *self) {
    GString *ret = g_string_new(NULL);
    
    for (GList *value = self; value != NULL; value = value->next) {
        (void)g_string_append_printf(ret, ",%i", GPOINTER_TO_INT(value->data));
    }
    
    (void)g_string_overwrite(ret, 0, "(");
    (void)g_string_append(ret, ")");
    return g_string_free(ret, FALSE);
}
