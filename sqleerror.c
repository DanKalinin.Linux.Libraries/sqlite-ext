//
// Created by dan on 31.10.2019.
//

#include "sqleerror.h"

G_DEFINE_QUARK(sqle-error-quark, sqle_error)

GError *sqle_error_new(gint code) {
    gchar *message = (gchar *)sqlite3_errstr(code);
    GError *self = g_error_new_literal(SQLE_ERROR, code, message);
    return self;
}

void sqle_set_error(GError **self, gint code) {
    gchar *message = (gchar *)sqlite3_errstr(code);
    g_set_error_literal(self, SQLE_ERROR, code, message);
}
